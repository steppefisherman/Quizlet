package com.example.quiz.screens

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.quiz.R
import com.example.quiz.databinding.FragmentStartBinding
import com.example.quiz.utils.APP_ACTIVITY

class FragmentStart : Fragment() {
    private var binding: FragmentStartBinding? = null
    private val mBinding get() = binding!!
    lateinit var btnStart: Button

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentStartBinding.inflate(layoutInflater, container, false)
        APP_ACTIVITY.title = getString(R.string.title_start)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initialise()
    }

    private fun initialise() {
        btnStart = mBinding.btnStart
        btnStart.setOnClickListener {
            APP_ACTIVITY.navController
                .navigate(R.id.action_fragmentStart_to_fragment1)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}