package com.example.quiz.screens

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import com.example.quiz.R
import com.example.quiz.databinding.Fragment10Binding
import com.example.quiz.utils.APP_ACTIVITY
import com.example.quiz.utils.count


class Fragment10 : Fragment() {
    private var binding: Fragment10Binding? = null
    private val mBinding get() = binding!!
    private lateinit var btnYes: Button
    private lateinit var btnNo: Button
    private var mCurrentCount: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = Fragment10Binding.inflate(layoutInflater, container, false)
        mCurrentCount = arguments?.getInt(count) ?: 0
        APP_ACTIVITY.title = getString(R.string.title_10)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnYes = mBinding.btnYes
        btnNo = mBinding.btnNo
        Log.d("AAA", "mCurrentCount_received: $mCurrentCount")
        sendPositiveResult()
        sendNegativeResult()
    }

    private fun sendPositiveResult() {
        btnYes.setOnClickListener {
            mCurrentCount++
            val bundle = Bundle()
            bundle.putInt(count, mCurrentCount)
            APP_ACTIVITY.navController.navigate(R.id.action_fragment10_to_fragmentResult, bundle)
            Log.d("AAA", "mCurrentCount_sent: $mCurrentCount")
        }
    }

    private fun sendNegativeResult() {
        btnNo.setOnClickListener {
            val bundle = Bundle()
            bundle.putInt(count, mCurrentCount)
            APP_ACTIVITY.navController.navigate(R.id.action_fragment10_to_fragmentResult, bundle)
            Log.d("AAA", "mCurrentCount_sent: $mCurrentCount")
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
        btnNo.setOnClickListener(null)
    }
}