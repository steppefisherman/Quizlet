package com.example.quiz.screens

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.example.quiz.R
import com.example.quiz.databinding.FragmentResultBinding
import com.example.quiz.utils.APP_ACTIVITY
import com.example.quiz.utils.count


class FragmentResult : Fragment() {
    private var binding: FragmentResultBinding? = null
    private val mBinding get() = binding!!
    private lateinit var btnYes: Button
    private lateinit var textViewResult: TextView
    private var mCurrentCount: Int = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = FragmentResultBinding.inflate(layoutInflater, container, false)
        mCurrentCount = arguments?.getInt(count) ?: 0
        APP_ACTIVITY.title = getString(R.string.title_result)
        return mBinding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        btnYes = mBinding.btnYes
        textViewResult = mBinding.textViewResult
        Log.d("AAA", "mCurrentCount_received: $mCurrentCount")
        showResults()
        startNewQuiz()
    }

    private fun showResults() {
        val zeroRange = "Хммм, слабовато, у вас нет верных ответов"
        val firstRange = "Нууу, уже не плохо, у вас $mCurrentCount верный ответ"
        val secondRange = "Хорошо идете, $mCurrentCount верных ответа"
        val thirdRange = "Хорошо идете, $mCurrentCount верных ответов"
        val fourthRange = "Впечатляет, $mCurrentCount верных ответов"
        val lastRange = "Браво! Перфекто! 10 из 10."
        when (mCurrentCount) {
            0 -> textViewResult.text = zeroRange
            1 -> textViewResult.text = firstRange
            in 2..4 -> textViewResult.text = secondRange
            in 5..7 -> textViewResult.text = thirdRange
            in 8..9 -> textViewResult.text = fourthRange
            10 -> textViewResult.text = lastRange
        }
    }

    private fun startNewQuiz() {
        btnYes.setOnClickListener {
            APP_ACTIVITY.navController.navigate(R.id.action_fragmentResult_to_fragmentStart)
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}